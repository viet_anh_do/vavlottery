package lottery;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Anh on 1/22/2016.
 */
public class LiveLotteryResult extends LotteryInfo {
    @Override
    public LotteryResult getResult(String area, Date... date) {
        area = area.trim().toLowerCase();
        if( area.length() == 0 ) return northResult();
        else
        if( area.charAt(0) == 't' ) return midResult();
        else
        if( area.charAt(0) == 'n' ) return southResult();
        return northResult();
    }

    private LotteryResult midResult() {
        String url = "http://xskt.com.vn/ttttxs-p.jsp?areaCode=MT";
        LotteryResult lotResult = new LotteryResult();
        try {
            Document doc = Jsoup.connect(url).get();
            Element tbody = doc.select("table[class=tbl-xsmn]").get(0).select("tbody").get(0);
            String[] provinces = new String[tbody.child(1).children().size()];
            for(int i = 0; i < tbody.child(1).children().size(); i++) {
                provinces[i] = tbody.child(1).child(i).text();
                lotResult.put(provinces[i], new String[9]);
            }
            Element tr;
            for(int index = 2, p_id = 8; index < tbody.children().size(); index++, p_id--) {
                tr = tbody.child(index);
                for(int idx = 1; idx < tr.children().size(); idx ++) {
                    lotResult.get(provinces[idx-1])[p_id] = tr.child(idx).text();
                }
            }
        }catch(IOException e) {
            e.printStackTrace();
        }
        return lotResult;
    }

    private LotteryResult northResult() {
        String url = "http://xskt.com.vn/ttttxs-p.jsp?areaCode=MB";
        LotteryResult lotResult = new LotteryResult();
        try {
            Document doc = Jsoup.connect(url).get();
            Element tbody = doc.select("table[class=result]").get(0).select("tbody").get(0);
            lotResult.put("Xổ số miền Bắc", new String[8]);
            String title;
            Element tr;
            for(int index = 1, idx = 0; index < tbody.children().size(); index++) {
                tr = tbody.child(index);
                title = tr.child(0).text();
                if( title.length() == 0 || ( title.charAt(0) >= '0' && title.charAt(0) <= '9')) continue;
                lotResult.get("Xổ số miền Bắc")[idx++] = tr.child(1).text();
            }
        }catch(IOException e) {
            e.printStackTrace();
        }
        return lotResult;
    }

    private LotteryResult southResult() {
        String url = "http://xskt.com.vn/ttttxs-p.jsp?areaCode=MN";
        LotteryResult lotResult = new LotteryResult();
        try {
            Document doc = Jsoup.connect(url).get();
            Element tbody = doc.select("table[class=tbl-xsmn]").get(0).select("tbody").get(0);
            String[] provinces = new String[tbody.child(1).children().size()];
            for(int i = 0; i < tbody.child(1).children().size(); i++) {
                provinces[i] = tbody.child(1).child(i).text();
                lotResult.put(provinces[i], new String[9]);
            }
            Element tr;
            for(int index = 2, p_id = 8; index < tbody.children().size(); index++, p_id--) {
                tr = tbody.child(index);
                for(int idx = 1; idx < tr.children().size(); idx ++) {
                    lotResult.get(provinces[idx-1])[p_id] = tr.child(idx).text();
                }
            }
        }catch(IOException e) {
            e.printStackTrace();
        }
        return lotResult;
    }
}
