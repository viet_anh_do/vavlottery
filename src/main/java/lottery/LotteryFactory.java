package lottery;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

/**
 * Created by Anh on 1/22/2016.
 */
public class LotteryFactory {
    // Bac 18:15
    // Nam 16:20
    // Trung 17:15
    private long openedNorthTime = 1L*((18 * 60  + 15) * 60 ) * 1000;
    private long openedMidTime = 1L*((17 * 60 + 15) * 60) * 1000;
    private long openedSouthTime = 1L*((16 * 60 + 20) * 60) * 1000;
    private long aMinuteInMilis = 60L*1000;
    private long adayInMillis = 24L*60*aMinuteInMilis;
    private boolean inTheSameDay(Date date1, Date date2) {
        SimpleDateFormat fmt = new SimpleDateFormat("yMd");
        return fmt.format(date1).equals(fmt.format(date2));
    }
    private int inRange(long value, long left, long right) {
        return ( value < left ) ? -1 : ( value > right ? 1 : 0 );
    }
    private long getOpenedTime(String area) {
        area = area.trim().toLowerCase();
        if( area.length() == 0 ) return openedNorthTime;
        else
        if( area.charAt(0) == 't' ) return openedMidTime;
        else
        if( area.charAt(0) == 'n' ) return openedSouthTime;
        return openedNorthTime;
    }
    // time at O0:00:00 in new Date(0)
    private long timeFromMidNight(Date date) {
        return date.getTime() + 7L*60*60*1000;
    }
    public LotteryResult getLotteryResult(String area, Date date) throws InvalidLotteryDate {
        Date now = new Date();
        LotteryInfo lotteryInfo = null;
        long openedTime = getOpenedTime(area);
        if( inTheSameDay(now, date)) {
            if( inRange(timeFromMidNight(now) % adayInMillis, openedTime, openedTime + 30*aMinuteInMilis) == 0) {
                lotteryInfo = new LiveLotteryResult();
            }
            else if(timeFromMidNight(now) % adayInMillis < openedTime ) throw new InvalidLotteryDate("Chưa có kết quả");
            else lotteryInfo = new OldLotteryResult();
        }
            else
            {
                long oldestDay;
                if( timeFromMidNight(now) % adayInMillis >= openedTime ) {
                    oldestDay = timeFromMidNight(now) / adayInMillis - 4;
                }
                    else
                    {
                        oldestDay = timeFromMidNight(now) / adayInMillis - 5;
                    }
                int inRange = inRange(timeFromMidNight(date)/adayInMillis, oldestDay, oldestDay + 4);
                if( inRange == -1 )throw new InvalidLotteryDate("Mình đã không còn nhớ kết quả ngày hôm đó");
                if( inRange == 1 )throw new InvalidLotteryDate("Chưa có kết quả");
                lotteryInfo = new OldLotteryResult();
            }
        return lotteryInfo.getResult(area, date);
    }
}
