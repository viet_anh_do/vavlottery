package lottery;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

/**
 * Created by Anh on 1/17/2016.
 */
public class Main {
    public static void fetchPage(String url, String output) {
        Document doc = null;
        FileWriter writer = null;
        try{
            doc = Jsoup.connect(url)
                       .get();
//            System.out.println(doc.select("div[id^=ketqua"));
//            Elements div = doc.select("div[class=span-10]");
//            Element ele = div.get(1);
//            System.out.println(div.size());
//            Elements titles = doc.select(".tc.f2");
//            System.out.println(titles.size());
//            Elements tables = doc.select("div[id^=ketqua]");
//            for(int i = 0; i < titles.size(); i++) {
//                System.out.println(titles.get(i).child(0).text());
//                System.out.println(tables.get(i).select("tbody"));
//            }
//            Element channel = doc.select("channel").get(0);
//            System.out.println(channel);
//            System.out.println(doc.select("table").get(0));
            Element tbody = doc.select("table").get(0).select("tbody").get(0);
            for(Element tr : tbody.children() ) {
                for(Element td : tr.children()) {
                    System.out.println(td.text());
                    System.out.println("-------------------------");
                }
            }
            writer = new FileWriter(output);
            writer.write(doc.toString());
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Error");

        }finally {
            if( writer != null )
            {
                try{
                    writer.close();
                }catch (IOException ee){

                }
            }
        }
    }
    public static void test() {
        FileWriter write = null;
        try{
            write = new FileWriter("abc.html");
            return;
        }catch(IOException e) {
            e.printStackTrace();
        }finally {
            System.out.println("finally");
        }
    }
    public static void print(LotteryResult info) {
        for(String area : info.keySet()) {
            System.out.println(area);
            String[] numbers = info.get(area);
            for(int i = 0; i < numbers.length; i++) {
                if( numbers[i] == null ) break;
                System.out.println(i+ ":" +numbers[i]);
            }
        }
    }
    public static void testString(String a) {
        a = "a";
    }
    public static void main(String []args) {
        Date today = new Date();
        long adayInMillis = 24L*60*60*1000;
        Date adayago = new Date(System.currentTimeMillis() - adayInMillis);
        Date twodayago = new Date(System.currentTimeMillis() - 2*adayInMillis);
        Date tomorrow = new Date(System.currentTimeMillis() + adayInMillis);
        LotteryFactory lotteryFactory = new LotteryFactory();
        long start = System.currentTimeMillis();

        try {
            SimpleDateFormat ft = new SimpleDateFormat("y-M-d H");
            today = ft.parse("2016-01-18 17");
//            System.out.println(today);
//            System.out.println();
//            print(lotteryFactory.getLotteryResult("bac", twodayago));
//            print(lotteryFactory.getLotteryResult("trung", twodayago));
//            print(lotteryFactory.getLotteryResult("nam", twodayago));
//            print(lotteryFactory.getLotteryResult("bac", adayago));
//            print(lotteryFactory.getLotteryResult("trung", adayago));
//            print(lotteryFactory.getLotteryResult("nam", adayago));
            print(lotteryFactory.getLotteryResult("bac", today));
            print(lotteryFactory.getLotteryResult("trung", today));
            print(lotteryFactory.getLotteryResult("nam", today));
        }catch (InvalidLotteryDate e) {
            System.out.println("Exception: " + e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long finish = System.currentTimeMillis();
        System.out.println((finish - start) + " millisecond(s)" );
    }
}
