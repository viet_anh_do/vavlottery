package lottery;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Anh on 1/22/2016.
 */
public class OldLotteryResult extends LotteryInfo {
    public LotteryResult getResult(String area, Date... dateList) {
        Date date = dateList[0];
        area = area.trim().toLowerCase();
        if( area.length() > 0 ) {
            if (area.charAt(0) == 't') return midResult(date);
            else if (area.charAt(0) == 'n') return southResult(date);
            else return northResult(date);
        }
        return northResult(date);
    }

    public String[] toCSV(String str) {
        str = String.join("", str.split("\\p{Space}"));
        str += "@";
//        System.out.println(str);
        String[] ret = new String[10];
        for(int i = 2, index = -1; i < str.length(); i++) {
            if( str.charAt(i) == ':' ) {
                ret[++index] = "";
                i++;
            }
                else ret[index] += str.charAt(i-1);
        }
        return ret;
    }

    private LotteryResult southResult(Date date) {
        LotteryResult lotResult = new LotteryResult();
        String url = "http://xskt.com.vn/rss-feed/mien-nam-xsmn.rss";
        try{
            Document doc = Jsoup.connect(url).get();
            String title, descript;
            for(Element item : doc.select("item")) {
                title = item.select("title").get(0).text();
                if( title.indexOf(String.format("%td/%<tm", date)) >= 0) {
                    descript = item.select("description").get(0).text();
                    for(String des : descript.split("\\[")) {
                        String[] desList = des.split("\\]");
                        if(desList.length < 2 ) continue;
                        lotResult.put(desList[0], toCSV(desList[1]));
                    }
                    return lotResult;
                }
            }
        }catch(IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private LotteryResult midResult(Date date) {
        LotteryResult lotResult = new LotteryResult();
        String url = "http://xskt.com.vn/rss-feed/mien-trung-xsmt.rss";
        try{
            Document doc = Jsoup.connect(url).get();
            String title, descript;
            for(Element item : doc.select("item")) {
                title = item.select("title").get(0).text();
                if( title.indexOf(String.format("%td/%<tm", date)) >= 0) {
                    descript = item.select("description").get(0).text();
                    for(String des : descript.split("\\[")) {
                        String[] desList = des.split("\\]");
                        if(desList.length < 2 ) continue;
                        lotResult.put(desList[0], toCSV(desList[1]));
                    }
                    return lotResult;
                }
            }
        }catch(IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private LotteryResult northResult(Date date) {
        LotteryResult lotResult = new LotteryResult();
        String url = "http://xskt.com.vn/rss-feed/mien-bac-xsmb.rss";
        try {
            Document doc = Jsoup.connect(url).get();
            String title;
            for(Element item : doc.select("item")) {
                title = item.select("title").get(0).text();
                if( title.indexOf(String.format("%td/%<tm", date)) >= 0) {
                    lotResult.put("Xổ số miền Bắc", toCSV(item.select("description").get(0).text()));
                    return lotResult;
                }
            }
        }catch(IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
