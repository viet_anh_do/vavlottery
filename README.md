## How to use:

  - Method: LotteryResult getResult(String area, Date date) 
  - Params: 
      - area
      - date
  - Returns: a LotteryResult object ( that is extended from Hash<String, String[]>)
      - e.g:          
```
#!java

             {
               "0": ["12345"]
               "1": ["23456", "34567"]
               ...
               "7": [..........]
             }
```